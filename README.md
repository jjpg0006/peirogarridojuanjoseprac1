# PeiroGarridoJuanJosePrac1

**Análisis:**

**Datos:**

TDA Lista(escena) (dispondremos de 3)
Almacena en orden FIFO elementos del escena
operaciones
add(TipoElemento) : inserta al final de la Lista un elemento.
remove(): devuelve y elimina el primer elemento de la Lista. 
size(): nos devuelve el número de elementos en la lista.
isEmpty() nos dice si la lista está vacía o no.

TDA Lista(GeneradorEscena) (usa los métodos descritos arriba)

TDA Lista(RenderizadorEscena) (usa los métodos descritos arriba)

TDA Future<Integer>
	operaciones
	.get(número) nos devuelve la posición número de la lista
	.get() nos devuelve el elemento futuro

4 Semáforos
.acquire() para intentar coger un permiso del semáforo.
.release() para devolver un permiso al semáforo.

TDA Fotograma
	String:id
	procedimiento:
	toString()

TDA Escena
id: String
tiempoescena: Entero
prioridad: Booleano
fotogramas: Vector de fotogramas
ex: Semaforo
horaGeneracion: Lista de enteros
horaReinderizadorComienzo: Lista de enteros
horaReinderizadorFin: Lista de enteros

procedimientos:
	setTiempoescena(),getTiempoescena(),setHoraGeneracion(),
	sethoraReinderizadorComienzo(),sethoraReinderizadorFin(),
	HoraGeneracionString(),HoraReinderizadorComienzo(),HoraReinderizadorFin(),
	MuestraFotogramas(),calcularTiempoTotal()



TDA ExecutionService
.execute() para mandar al marco de ejecución un Generador o Renderizador para que se ponga a hacer su método call/run.



TDA GeneradorEscena

ex: semáforo

numejecuciones : entero

numejecucionesdevolver : entero

VariablesCompartidas(Con RenderizadorEscena)

exprioridades : Semaforo

exnoprioridades : Semaforo

escenaprioritaria : Lista de escenas

escenanoprioritaria : Lista de escenas

procedimientos:
-Call()
-generarEscena (explicados más adelante)

Generará las escenas.En esta clase se da la responsabilidad de llamar a los sethorageneracion para saber cuando se ha creado una Escena, además se llamará a setTiempoescena() despues de crearlo para calcular el tiempo de sus fotogramas, además será la responsable de escribir en las listas de escenas según la prioridad. También cuando cree una escena reducirá en uno los huecos libres de la lista donde se cree con un adquire en el semáforo respectivo.


TDA RenderizadorEscena
ex: semáforo
resultado : Lista de Escenas (se compartira con el Main)
end : booleano
exprioridadesnosuperiormaximo : semaforo
exnoprioridadesnosuperiormaximo : semaforo

Renderizará las escenas y sacará de las listas de escenas las escenas ya renderizadas y las meterá en la lista de resultados que comparte con el main. Al acabar de renderizar un elemento de una lista, invocará al procedimiento release del semáforo posteriormente indicado para saber que se puede meter un elemento más o desbloquear la lista.





**Variables Compartidas:**
escenaprioritaria y escenanoprioritaria : buffer Escenas. Almacenará las escenas creadas por  GeneradorEstado y sacara las reinderizadas RenderizadorEscenas

exprioridades y exnoprioridades : semáforos para controlar que no se entre en las listas escenaprioritaria y no prioritaria por más de un Renderizador a la vez.

**Semáforos:**

Semaforo ex clase Escena, se crea inicializado a 0 y se libera cuando GeneradorEscena crea la escena, sirve para que solo se pueda usar una escena por RenderizadorEscenas.

Semaforo ex clase GeneradorEscena, se crea inicializando a 1, vale para que un generadorEscena cree una Escena a la vez y no se creen varias a la vez, se adquiere al principio del método call y se libera al final.

Semaforo exprioridad y semaforo exnoprioridad ReenderizadorEscenas son semáforos para controlar que no se entre en las listas escenaprioritaria y no prioritaria por más de un Renderizador a la vez.

Semaforo exprioridadesnosuperiormaximo y exnoprioridadesnosuperiormaximo clases GeneradorEscena y ReenderizadorEscenas  se inicializan con el valor  MAX_ESCENAS_EN_ESPERA, sirve para cuando se cree una escena, se adquiera el semáforo y cuando se renderiza una se libera para que asi no haya más de 3 en la cola.


**Procedimientos de apoyo:**

Fotogramas:
toString(): Servirá para mostrar el nombre del fotograma y su tiempo de duración para luego imprimirlo en el main.

Escena:
setTiempoEscena(): Servirá para hacer el cálculo de todos los tiempos de los fotogramas y añadirle la constante para calcular el tiempo que se requiere en hacer una escena.

setHoraGeneracion(),sethoraReinderizadorComienzo() y sethoraReinderizadorFin(), servirán para calcular la hora cuando se generó la Escena, cuando empezó a Reinderizarse y cuando acabó de Reinderizarse, se llamarán a estas funciones en el momento deseado.

HoraGeneracionString(),HoraReinderizadorComienzo(),HoraReinderizadorFin() serviran para pasar a string las horas previamente calculadas para luego ser imprimidas por pantalla.

MuestraFotogramas(), Dispondrá de un bucle para mostrar sus diferentes fotogramas llamando al método toString de los fotogramas.

calcularTiempoTotal(), Calculará el tiempo que se ha pasado un reinderizador en procesar una escena.

GeneradorEscena:
generarEscena() sirve para crear una Escena, se creará con prioridad aleatoria.

call() llama al método generarEscena() tantas veces como número de Escenas se le haya asignado previamente en el main, dispondrá de un semáforo para que se vayan creando de una en una y devolverá el número de ejecuciones que ha realizado.


ReenderizadorEscena:
reenderizarPrioridad() y reenderizarnoPrioridad() vale para procesar una escena según sea de prioridad o no sea de prioridad,tendrán dos candados para que no se rendericen dos veces la misma escena también indicamos al semáforo que lleva la cuenta de los elementos que pueden haber en la lista, que libere un espacio.

run() llamará a los métodos reenderizarPrioridad y reenderizarnoPrioridad() , los llamará por orden de preferencia, hasta que no se renderizan los de Prioridad no se renderizan los de no prioridad y dispondrá de un semáforo para que se vayan renderizando de una en una.

ProgramaPrincipal
while(totalescenas!=resultado.size()), esperaremos a que todos los renderizadores hayan acabado y luego pondremos el setEnd() de los renderizados a true para que paren.

**Diseño:**

Fotogramas
Variables locales:
id: String
tiempocalculo: Entero

Constantes:
MIN_TIEMPO_FOTOGRAMA = 3
VARIACION_TIEMPO = 2
```

 public Fotogramas(String id) {
        this.id=id;
        this.tiempocalculo= NumeroAleatorio(VARIACION_TIEMPO)  +MIN_TIEMPO_FOTOGRAMA;  }
```

Escena

id: String

tiempoescena: Entero

prioridad: Booleano

fotogramas: Vector de fotogramas

ex: Semaforo

horaGeneracion: Lista de enteros

horaReinderizadorComienzo: Lista de enteros

horaReinderizadorFin: Lista de enteros

Constantes:
MIN_NUM_FOTOGRAMAS = 2
VARIACION_NUM_FOTOGRAMAS = 5
TIEMPO_FINALIZACION_ESCENA = 1
```
publico : Escena(id: , prioridad: Booleano) { // Constructor
        this.ex= nuevo semaforo inicializado a 0
        this.id = id
        this.prioridad = prioridad
        this.fotogramas = Vector(NumeroAleatorio(VARIACION_TIEMPO)+MIN_NUM_FOTOGRAMAS)      this.tiempoescena=
        this.horaGeneracion= Nuevo ArrayList
        this.horaReinderizadorComienzo=Nuevo ArrayList
        this.horaReinderizadorFin=Nuevo ArrayList }

vacio ,publico : setTiempoescena() {
	variables locales
	aux : entero

        for (int i=0;i<this.fotones.size();i++){
        aux=aux+this.fotones.get(i).getTiempocalculo()
        }
        aux=aux+TIEMPO_FINALIZACION_ESCENA
        this.tiempoescena = aux
    }

publico,entero:  getTiempoescena() {
        return tiempoescena
    }

 vacio: setHoraGeneracion(){
//Variables locales
hours,minutes,seconds : enteros
localDate : LocalDateTime

    localDate = LocalDateTime.now()
     hours  = localDate.getHour()
    minutes = localDate.getMinute()
    seconds = localDate.getSecond()

// Añadimos los elementos al vector para luego convertirlos en string    
    horaGeneracion.add(hours);
    horaGeneracion.add(minutes);
    horaGeneracion.add(seconds); }

String :  HoraGeneracionString(){
//Variables locales
hours,minutes,seconds : enteros


     hours  = horaGeneracion.get(0)
     minutes = horaGeneracion.get(1)    
     seconds = horaGeneracion.get(2)
    return ("Hora Generacion : " + hours + ":"+ minutes+":"+seconds)
    
    }



 vacio: sethoraReinderizadorComienzo(){
//Variables locales
hours,minutes,seconds : enteros
localDate : LocalDateTime

    localDate = LocalDateTime.now()
     hours  = localDate.getHour()
    minutes = localDate.getMinute()
    seconds = localDate.getSecond()

// Añadimos los elementos al vector para luego convertirlos en string    
   horaReinderizadorComienzo.add(hours)
    horaReinderizadorComienzo.add(minutes)
   horaReinderizadorComienzo.add(seconds)    
     }


String :  HorahoraReinderizadorComienzo(){
//Variables locales
hours,minutes,seconds : enteros


      horaReinderizadorComienzo = horaGeneracion.get(0)
     horaReinderizadorComienzo = horaGeneracion.get(1)    
      horaReinderizadorComienzo = horaGeneracion.get(2)
    return ("Hora Comienzo : " + hours + ":"+ minutes+":"+seconds)
    
    }

//Se haria igual para la hora horaReinderizadorFin

vacio: MuestraFotogramas() {
       for (int i=0;i<this.fotones.size();i++){
       System.out.println(fotones.get(i).toString())       }
    }

// Devolvemos el tiempo en segundos del tiempo que ha pasado reinderizando
int calcularTiempoTotalReinderizando(){
//variables locales
sumatoriafinal, sumatoriainicial: enteros

     sumatoriafinal=0;
     sumatoriafinal  = sumatoriafinal + horaReinderizadorFin.get(0)*3600
     sumatoriafinal = sumatoriafinal +horaReinderizadorFin.get(1)*60
     sumatoriafinal = sumatoriafinal+horaReinderizadorFin.get(2)
     
     sumatoriainicial=0;
     sumatoriainicial  = sumatoriainicial + horaReinderizadorComienzo.get(0)*3600
     sumatoriainicial = sumatoriainicial +horaReinderizadorComienzo.get(1)*60
     sumatoriainicial = sumatoriainicial +horaReinderizadorComienzo.get(2)
     
     return sumatoriafinal-sumatoriainicial
     
     }
```

GeneradorEscena : Implementa Callable con parametro de entero

VariablesLocales

ex: semáforo

numejecuciones : entero

numejecucionesdevolver : entero

VariablesCompartidas(Con RenderizadorEscena)

exprioridades : Semaforo

exnoprioridades : Semaforo

escenaprioritaria : Lista de escenas

escenanoprioritaria : Lista de escenas


Constantes
MIN_NUM_GENERACION = 3
VARIACION_TIEMPO = 3
```

   void: generarEscena() que lanza InterruptedException{
    //Variables locales
	rand:Random
	aux:Boolean
	escena:Escena

    rand = new Random();
    aux=rand.nextBoolean();
    
    escena =new Escena("hola",aux);
    escena.ex.release();
    
    for (int i=0;i < escena.fotones.capacity();i++){
        Fotogramas uno=new Fotogramas("Fotograma "+ i);
        escena.fotones.add(uno);
        }
    
    escena.setTiempoescena(); 
    escena.setHoraGeneracion()
    
   	 if(aux==true){
   	 escenaprioritaria.add(escena)
    	exprioridades.acquire()  }
    
  	 if(aux==false){
    	escenanoprioritaria.add(escena)
    	exnoprioridades.acquire()}
 }



   publico,entero : call() throws Exception {
        while (numejecuciones>0){
       
 try {
            ex.acquire()
        } catch (InterruptedException ex) {
            Logger.getLogger(GeneradorEscena.class.getName()).log(Level.SEVERE, null, ex);
        }
        
               generarEscena();
        
   //Simulamos el tiempo de espera
        
 try {
        int duration=Numero aleatorio (VARIACION_TIEMPO+1) + MIN_NUM_GENERACION;
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
        e.printStackTrace()
    }

    ex.release();
  
    numejecuciones--;
        }
 return numejecucionesdevolver;
    }

```
RenderizadorEscena : Implementa Runnable
//VariablesLocales

ex: semáforo

resultado : Lista de Escenas (se compartira con el Main)

end : booleano

exprioridadesnosuperiormaximo : semaforo

exnoprioridadesnosuperiormaximo : semaforo

VariablesCompartidas(Con RenderizadorEscena)

exprioridades : Semaforo

exnoprioridades : Semaforo

escenaprioritaria : Lista de escenas

escenanoprioritaria : Lista de escenas

```
publica,vacia run() {

        while(end!=true){
        
        try {
            ex.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(GeneradorEscena.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (escenaprioritaria.isEmpty() != true) {
            
            try {
                exprioridad.acquire();
                reenderizarPrioridad();
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizadorEscena.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("reenderizando prioridad");
            
            
        } else {
            if (escenanoprioritaria.isEmpty() != true) {
                try {
                    exnoprioridad.acquire();
                    reenderizarnoPrioridad();
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscena.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("reenderizando no prioridad");
            } else {}
            
        }

        ex.release();
        }
    }
vacio :reenderizarPrioridad() lanza InterruptedException {
        
        if (escenaprioritaria.isEmpty() != true){
        
        escenaprioritaria.get(0).ex.acquire();
         
        System.out.println("renderizando por " + escenaprioritaria.get(0).getTiempoescena());
        
        escenaprioritaria.get(0).sethoraReinderizadorComienzo();

        try {
            int duration = escenaprioritaria.get(0).getTiempoescena()
            TimeUnit.SECONDS.sleep(duration)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        escenaprioritaria.get(0).sethoraReinderizadorFin();
        resultado.add(escenaprioritaria.get(0));
        
        escenaprioritaria.get(0).ex.release();
        
        
        escenaprioritaria.remove(0);
        exprioridadesnosuperiormaximo.release();
        
        exprioridad.release();
        
    }}


vacio: reenderizarnoPrioridad() lanza  InterruptedException {
        
         if (escenanoprioritaria.isEmpty() != true){
        escenanoprioritaria.get(0).ex.acquire()        
        System.out.println("renderizando por" + escenanoprioritaria.get(0).getTiempoescena())
        escenanoprioritaria.get(0).sethoraReinderizadorComienzo();

        try {
            int duration = escenanoprioritaria.get(0).getTiempoescena()
            TimeUnit.SECONDS.sleep(duration)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        escenanoprioritaria.get(0).sethoraReinderizadorFin()
        resultado.add(escenanoprioritaria.get(0))
        escenanoprioritaria.get(0).ex.release()
        
        
        escenanoprioritaria.remove(0)
        exnoprioridadesnosuperiormaximo.release()
        exnoprioridad.release()
                
                }}


publico,vacio setEnd(end :Booleano) {
        this.end = end;
    }
```

 Programa Principal
Constantes:
    MAX_ESCENAS_ESPERA= 3;
    NUM_GENERADORES= 3;
    NUM_RENDERIZADORES= 3;
    MIN_NUM_ESCENAS= 2;
    VARIACION_NUM_ESCENAS= 2;

escenaprioritaria: Array List de Escenas

escenanoprioritaria : Array List de Escenas

resultado : Array List de Escenas

exprioridades : Semaforo capacidad (MAX_ESCENAS_ESPERA)

 exnoprioridades : Semaforo capacidad(MAX_ESCENAS_ESPERA)
 
aux1 : Semaforo capacidad 1

aux2 : Semaforo capacidad 1

taskList : Array List de GeneradordeEscenas

ejecucion : ExecutorService

totalescenas : int

resultList : Lista de futuros de enteros
```
for (int i=0;i<NUM_GENERADORES;i++){
     GeneradorEscena hola= new GeneradorEscena(escenaprioritaria,escenanoprioritaria,
             NUMEROALETORIO( VARIACION_NUM_ESCENAS+1) + MIN_NUM_ESCENAS,exprioridades,exnoprioridades)
     taskList.add(hola)
     }



 for (int i=0;i<NUM_RENDERIZADORES;i++){
     RenderizadorEscena hola2= new RenderizadorEscena(escenaprioritaria,escenanoprioritaria,resultado,exprioridades,exnoprioridades,aux1,aux2)
        ejecucion.execute(hola2)
        listaca.add(hola2)
     }

List<Future<Integer>>resultList=null
 try {
    resultList=ejecucion.invokeAll(taskList)
} catch (InterruptedException e) {
    e.printStackTrace()
}


 for (int i=0; i<resultList.size(); i++){
    Future<Integer> future=resultList.get(i);
    try {
        Integer result=future.get();
        totalescenas=totalescenas+result;
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    } }

ejecucion.shutdown()

//Ciclo para esperar a que todos los resultados se hayan procesado
while(totalescenas!=resultado.size()){ }

for (int i=0;i<listaca.size();i++){
    listaca.get(i).setEnd(Boolean.TRUE);}
    
    aux1.release(1);
    aux2.release(1);

System.out.println("El numero total de escenas procesadas es " + totalescenas)
     int totaltiemporeinderizador=0
     for (int i=0;i<resultado.size();i++){
         totaltiemporeinderizador=totaltiemporeinderizador+resultado.get(i).getTiempoescena()
     }
     System.out.println("El tiempo total que han trabajado entre todos los reinderizadores es " + totaltiemporeinderizador)


     for (int i=0;i<resultado.size();i++){
         System.out.println(resultado.get(i).HoraGeneracionString())
         System.out.println(resultado.get(i).HoraReinderizadorComienzo())
         System.out.println(resultado.get(i).HoraReinderizadorFin())
         resultado.get(i).MuestraFotogramas()
         System.out.println(resultado.get(i).calcularTiempoTotal())
     }      
       
```